﻿using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Reflection;

namespace PetShop.Blazor.DependencyInjection.Extensions
{
    public static class DIExtensions
    {
        public static void AddStates(this IServiceCollection services)
        {
            Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.Name.EndsWith("State"))
                .ToList().ForEach(t => services.AddScoped(t));
        }
    }
}
