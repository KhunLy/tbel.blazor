﻿namespace PetShop.Blazor.Models
{
    public class BreedModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int AnimalId { get; set; }

        public string AnimalName { get; set; }
    }
}
