﻿namespace PetShop.Blazor.Models
{
    public class PetStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
