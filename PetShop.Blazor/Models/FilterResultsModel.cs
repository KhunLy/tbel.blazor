﻿using System.Collections.Generic;

namespace PetShop.Blazor.Models
{
    public class FilterResultsModel<T>
    {
        public int Total { get; set; }

        public int FilterCount { get; set; }

        public int Limit { get; set; }

        public int Offset { get; set; }

        public IEnumerable<T> Results { get; set; }
    }
}
