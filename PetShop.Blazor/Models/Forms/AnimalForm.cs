﻿using System.ComponentModel.DataAnnotations;

namespace PetShop.Blazor.Models.Forms
{
    public class AnimalForm
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
