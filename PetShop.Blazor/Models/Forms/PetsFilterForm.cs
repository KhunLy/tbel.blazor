﻿namespace PetShop.Blazor.Models.Forms
{
    public class PetsFilterForm
    {
        public int? Limit { get; set; }
        public int? Offset { get; set; }
        public int? AnimalId { get; set; }
        public int? PetStatusId { get; set; }
        public int? BreedId { get; set; }
        public string ReferenceSearch { get; set; }
    }
}
