﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PetShop.Blazor.Models.Forms
{
    public class RegisterForm
    {
        [Required]
        [EmailAddress]
        [MaxLength(255)]
        public string Email { get; set; }

        [Required]
        [MaxLength(255)]
        public string Password { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        public DateTime? BirthDate { get; set; }
    }
}
