﻿using PetShop.Blazor.Validators;
using System;
using System.ComponentModel.DataAnnotations;

namespace PetShop.Blazor.Models.Forms
{
    public class PetEditForm
    {
        public int Id { get; set; }

        [Required]
        public string Reference { get; set; }

        [Required]
        [NotBeforeToday]
        public DateTime BirthDate { get; set; }

        [Required]
        public int BreedId { get; set; }


        public byte[] Image { get; set; }
        public string ImageMimeType { get; set; }

        [Required]
        public bool Vaccinated { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }
    }
}
