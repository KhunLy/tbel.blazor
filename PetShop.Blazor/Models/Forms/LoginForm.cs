﻿using System.ComponentModel.DataAnnotations;

namespace PetShop.Blazor.Models.Forms
{
    public class LoginForm
    {
        [Required]
        [MaxLength(255)]
        //[EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
