﻿using System.ComponentModel.DataAnnotations;

namespace PetShop.Blazor.Models.Forms
{
    public class BreedForm
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public int AnimalId { get; set; }
    }
}
