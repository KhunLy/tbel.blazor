﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace PetShop.Blazor.Interops
{
    public static class InteropExtensions
    {
        public static async Task ConsoleLogAsync(this IJSRuntime jSRuntime, object value)
        {
            await jSRuntime.InvokeVoidAsync("consolelog", value);
        }

        public static async Task ToastrSuccessAsync(this IJSRuntime jSRuntime, string message, string title)
        {
            await jSRuntime.InvokeVoidAsync("toastr.success", message, title);
        }

        public static async Task ToastrErrorAsync(this IJSRuntime jSRuntime, string message, string title)
        {
            await jSRuntime.InvokeVoidAsync("toastr.error", message, title);
        }

        public static async Task ToastrInfoAsync(this IJSRuntime jSRuntime, string message, string title)
        {
            await jSRuntime.InvokeVoidAsync("toastr.info", message, title);
        }

        public static async Task BsOpenModal(this IJSRuntime jSRuntime, string selector)
        {
            await jSRuntime.InvokeVoidAsync("bs.openModal", selector);
        }

        public static async Task BsCloseModal(this IJSRuntime jSRuntime, string selector)
        {
            await jSRuntime.InvokeVoidAsync("bs.closeModal", selector);
        }

        public static async Task<string> CropAsync(this IJSRuntime jSRuntime, ElementReference file, ElementReference cropper, ElementReference image)
        {
            return await jSRuntime.InvokeAsync<string>("cropAsync", file, cropper, image);
        }

        public static async Task TriggerClickAsync(this IJSRuntime jSRuntime, ElementReference element)
        {
            await jSRuntime.InvokeAsync<string>("trigger.click", element);
        }
    }
}
