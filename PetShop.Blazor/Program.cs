using Blazored.LocalStorage;
using Blazored.Modal;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using PetShop.Blazor.Abstractions.Interfaces;
using PetShop.Blazor.DependencyInjection.Extensions;
using PetShop.Blazor.Services;
using PetShop.Blazor.States;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PetShop.Blazor
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri("http://localhost:52769/api/") });
            builder.Services.AddStates();
            builder.Services.AddScoped<IJsonRequester, PetShopRequester>();
            builder.Services.AddScoped<JwtParser>();
            builder.Services.AddOptions();
            builder.Services.AddAuthorizationCore();
            builder.Services.AddScoped<AuthenticationStateProvider, AuthState>();
            builder.Services.AddBlazoredLocalStorage();
            builder.Services.AddBlazoredModal();

            await builder.Build().RunAsync();
        }
    }
}
