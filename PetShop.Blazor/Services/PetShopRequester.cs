﻿using Blazored.LocalStorage;
using PetShop.Blazor.Abstractions.Interfaces;
using PetShop.Blazor.Exceptions;
using PetShop.Blazor.States;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ToolBox.HttpJsonClient.Services;

namespace PetShop.Blazor.Services
{
    public class PetShopRequester : JsonRequester, IJsonRequester
    {
        private readonly AppState AppState;
        private readonly HttpClient Client;
        private readonly ILocalStorageService LocalStorage;

        public PetShopRequester(HttpClient client, AppState appState, ILocalStorageService localStorage) : base(client)
        {
            AppState = appState;
            LocalStorage = localStorage;
            Client = client;
        }

        protected override void IsUnsuccessStatusCode(HttpStatusCode code, string content)
        {
            throw new HttpResponseException(code, content);
        }

        public IJsonRequester AddBearer(string value)
        {
            AddHeader("Authorization", "Bearer " + value);
            return this;
        }

        protected async override Task BeforeSendAsync()
        {
            string token = await LocalStorage.GetItemAsync<string>("TOKEN");
            if (!string.IsNullOrEmpty(token) && !Client.DefaultRequestHeaders.Contains("Authorization"))
                AddBearer(token);
            AppState.IsLoading = true;
        }

        protected async override Task AfterSendAsync()
        {
            AppState.IsLoading = false;
            Client.DefaultRequestHeaders.Clear();
            await Task.FromResult(0);
        }
    }
}
