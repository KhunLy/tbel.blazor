﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;

namespace PetShop.Blazor.Services
{
    public class JwtParser
    {
        public ClaimsPrincipal GetClaimsIdentity(string token)
        {
            return string.IsNullOrEmpty(token)
                    ? new ClaimsPrincipal()
                    : new ClaimsPrincipal(new List<ClaimsIdentity> { new ClaimsIdentity(ParseClaimsFromJwt(token), "jwt") });
        }

        public IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
        {
            try
            {
                var payload = jwt.Split('.')[1];
                var jsonBytes = ParseBase64WithoutPadding(payload);
                var keyValuePairs = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);
                return keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value.ToString()));
            }
            catch (Exception e)
            {
                return new List<Claim>();
            }

        }

        private byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2: base64 += "=="; break;
                case 3: base64 += "="; break;
            }
            return Convert.FromBase64String(base64);
        }
    }
}
