﻿using Blazored.Modal;
using Microsoft.AspNetCore.Components;
using PetShop.Blazor.Pages;

namespace PetShop.Blazor.Shared
{
    public partial class ConfirmModal
    {
        [CascadingParameter] BlazoredModalInstance BlazoredModal { get; set; }
        
        public void Ok()
        {
            BlazoredModal.Close();
        }

        public void Cancel()
        {
            BlazoredModal.Cancel();
        }
    }
}
