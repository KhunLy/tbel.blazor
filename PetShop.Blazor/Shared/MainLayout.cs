﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using PetShop.Blazor.States;
using System;

namespace PetShop.Blazor.Shared
{
    public partial class MainLayout : LayoutComponentBase, IDisposable
    {
        [Inject]
        public AppState AppState { get; set; }

        [Inject]
        public ILocalStorageService LocalStorage { get; set; }

        public void Logout()
        {
            LocalStorage.SetItemAsync("TOKEN", string.Empty);
        }

        protected override void OnInitialized()
        {
            AppState.AppStateChanged += StateHasChanged;
        }

        public void Dispose()
        {
            AppState.AppStateChanged -= StateHasChanged;
        }
    }
}
