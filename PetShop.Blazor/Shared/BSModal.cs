﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.Blazor.Shared
{
    public partial class BSModal
    {
        [Parameter]
        public string Id { get; set; }

        [Parameter]
        public string Title { get; set; }


        [Parameter]
        public string Size { get; set; }

        [Parameter]
        public RenderFragment ChildContent { get; set; }
    }
}
