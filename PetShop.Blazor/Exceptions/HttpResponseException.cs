﻿using System;
using System.Net;

namespace PetShop.Blazor.Exceptions
{
    public class HttpResponseException : Exception
    {
        public HttpStatusCode StatusCode { get; }

        public HttpResponseException(HttpStatusCode code, string message)
            : base(message)
        {
            StatusCode = code;
        }
    }
}
