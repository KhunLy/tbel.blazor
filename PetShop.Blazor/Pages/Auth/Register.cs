﻿using PetShop.Blazor.Exceptions;
using PetShop.Blazor.Interops;
using PetShop.Blazor.Models.Forms;
using System;

namespace PetShop.Blazor.Pages.Auth
{
    public partial class Register : MasterComponentBase
    {
        public RegisterForm Model { get; set; }

        public Register()
        {
            Model = new RegisterForm();
        }

        public async void Send()
        {
            try
            {
                string token = await JsonClient.PostAsync<RegisterForm, string>("user", Model);
                await JSRuntime.ToastrSuccessAsync("Registration OK", "");
                Navigation.NavigateTo("/auth/login");
            }
            catch (HttpResponseException e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, e.StatusCode.ToString());
            }
            catch (Exception e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, "Unknown Exception");
            }
        }
    }
}
