﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using PetShop.Blazor.Exceptions;
using PetShop.Blazor.Interops;
using PetShop.Blazor.Models.Forms;
using System;

namespace PetShop.Blazor.Pages.Auth
{
    public partial class Login : MasterComponentBase
    {
        public LoginForm Model { get; set; }

        [Inject]
        protected ILocalStorageService LocalStorage { get; set; }

        [Inject]
        protected AuthenticationStateProvider AuthState { get; set; }

        public Login()
        {
            Model = new LoginForm();
        }

        public async void Send()
        {
            try
            {
                string token = await JsonClient.PostAsync<LoginForm, string>("security/login", Model);
                await LocalStorage.SetItemAsync("TOKEN", token);
                AuthenticationState p = await AuthState.GetAuthenticationStateAsync();
                if (p.User.Identity.IsAuthenticated)
                {
                    await JSRuntime.ToastrInfoAsync("Bienvenue à vous", "Welcome !!");
                    if (p.User.IsInRole("ADMIN"))
                        Navigation.NavigateTo("/admin");
                    else
                        Navigation.NavigateTo("/");
                }
            }
            catch (HttpResponseException e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, e.StatusCode.ToString());
            }
            catch (Exception e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, "Unknown Exception");
            }
        }
    }
}
