﻿using PetShop.Blazor.Models;
using PetShop.Blazor.Models.Forms;
using PetShop.Blazor.Services.Extensions;
using PetShop.Blazor.States;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PetShop.Blazor.Pages.Admin
{
    public partial class Pet : MasterComponentBase
    {
        public FilterResultsModel<PetModel> Items { get; set; }

        public IEnumerable<PetStatusModel> PetStatuses { get; set; }

        public IEnumerable<AnimalModel> Animals { get; set; }

        public IEnumerable<BreedModel> Breeds { get; set; }

        public PetsFilterForm PetsFilter { get; set; }

        public Pet()
        {
            PetsFilter = new PetsFilterForm();
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            Animals = ModelState.Animals;
            Breeds = ModelState.Breeds;
            PetStatuses = ModelState.PetStatuses;
        }

        protected override async Task OnInitializedAsync()
        {
            Items = await JsonClient.GetAsync<FilterResultsModel<PetModel>>("Pet");
        }

        protected override void OnModelStateChanged()
        {
            Animals = ModelState.Animals;
            Breeds = ModelState.Breeds;
            PetStatuses = ModelState.PetStatuses;
            StateHasChanged();
        }

        public async void Remove(PetModel pet)
        {

        }

        public async void Filter()
        {
            Items = await JsonClient.GetAsync<FilterResultsModel<PetModel>>("Pet".AddQueryParams(PetsFilter));
            StateHasChanged();
        }
    }
}
