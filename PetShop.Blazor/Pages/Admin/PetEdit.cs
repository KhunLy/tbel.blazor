﻿using Microsoft.AspNetCore.Components;
using PetShop.Blazor.Exceptions;
using PetShop.Blazor.Interops;
using PetShop.Blazor.Models.Forms;
using System;
using System.Text;
using System.Threading.Tasks;

namespace PetShop.Blazor.Pages.Admin
{
    public partial class PetEdit : MasterComponentBase
    {
        public ElementReference InputFile { get; set; }
        public ElementReference CropperContainer { get; set; }
        public ElementReference Image { get; set; }

        public string Title { get { return Id == 0 ? "New Pet" : "Update " + Item.Reference; } }

        public PetEditForm Item { get; set; }

        [Parameter]
        public int Id { get; set; }

        public PetEdit()
        {
            Item = new PetEditForm();
        }

        protected override async Task OnInitializedAsync()
        {
            if (Id != 0)
            {
                Item = await JsonClient.GetAsync<PetEditForm>("pet/" + Id);
            }
            else
            {
                Item = new PetEditForm();
            }
        }

        public async void ImageClick()
        {
            await JSRuntime.TriggerClickAsync(InputFile);
        }

        public async void LoadFile()
        {
            try
            {
                string blob = await JSRuntime.CropAsync(InputFile, CropperContainer, Image);
                string[] array = blob.Split(",");
                Item.ImageMimeType = array[0].Replace("data:","").Replace(";base64","");
                Item.Image = Convert.FromBase64String(array[1]);
            }
            catch(Exception e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, "Unkown Error");
            }
        }

        public async void Send()
        {
            try
            {
                if(Id == 0)
                    await JsonClient.PostAsync("Pet", Item);
                else
                    await JsonClient.PutAsync("Pet", Item);
                await JSRuntime.ToastrSuccessAsync("OK", null);
                Navigation.NavigateTo("/admin/pet");
            }
            catch (HttpResponseException e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, e.StatusCode.ToString());
            }
            catch (Exception e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, "Unknown Error");
            }
        }
    }
}
