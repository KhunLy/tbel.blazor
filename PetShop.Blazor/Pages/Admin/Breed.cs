﻿using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;
using PetShop.Blazor.Exceptions;
using PetShop.Blazor.Interops;
using PetShop.Blazor.Models;
using PetShop.Blazor.Models.Forms;
using PetShop.Blazor.Shared;
using PetShop.Blazor.States;
using System;
using System.Collections.Generic;

namespace PetShop.Blazor.Pages.Admin
{
    public partial class Breed : MasterComponentBase
    {
        public IEnumerable<BreedModel> Items { get; set; }
        public IEnumerable<AnimalModel> Animals { get; set; }

        public BreedForm NewItem { get; set; }

        [Inject]
        private IModalService ModalService { get; set; }

        public Breed()
        {
            NewItem = new BreedForm();
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            Items = ModelState.Breeds;
            Animals = ModelState.Animals;
        }

        protected override void OnModelStateChanged()
        {
            Items = ModelState.Breeds;
            Animals = ModelState.Animals;
            StateHasChanged();
        }

        public async void Add()
        {
            try
            {
                await JsonClient.PostAsync("breed", NewItem);
                await JSRuntime.ToastrSuccessAsync($"{NewItem.Name} has been added", null);
                NewItem = new BreedForm();
                ModelState.RefreshModelStateAsync();
                await JSRuntime.BsCloseModal("#addBreedModal");
            }
            catch (HttpResponseException e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, e.StatusCode.ToString());
            }
            catch (Exception e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, "Unknown Exception");
            }
        }

        public async void Remove(BreedModel breed)
        {
            var modalRef = ModalService.Show<ConfirmModal>("Confirm delete ?");
            var result = await modalRef.Result;
            if (!result.Cancelled)
            {
                try
                {
                    await JsonClient.DeleteAsync("breed/" + breed.Id);
                    await JSRuntime.ToastrSuccessAsync($"{breed.Name} has been removed", null);
                    ModelState.RefreshModelStateAsync();
                }
                catch (HttpResponseException e)
                {
                    await JSRuntime.ToastrErrorAsync(e.Message, e.StatusCode.ToString());
                }
                catch (Exception e)
                {
                    await JSRuntime.ToastrErrorAsync(e.Message, "Unknown Exception");
                }
            }
        }
    }
}
