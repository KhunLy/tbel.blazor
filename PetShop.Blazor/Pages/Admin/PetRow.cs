﻿using Microsoft.AspNetCore.Components;
using PetShop.Blazor.Models;
using PetShop.Blazor.States;
using System.Collections.Generic;

namespace PetShop.Blazor.Pages.Admin
{
    public partial class PetRow : MasterComponentBase
    {
        [Parameter]
        public PetModel Item { get; set; }

        public int NewPetStatusId { get; set; }

        public IEnumerable<PetStatusModel> PetStatuses { get; set; }

        public string UpdateHref 
        { get { return "/admin/pet-edit/" + Item.Id; } }

        public bool HasChanged { get { return Item.PetStatusId != NewPetStatusId; } }

        protected override void OnInitialized()
        {
            PetStatuses = ModelState.PetStatuses;
            base.OnInitialized();
        }

        protected override void OnParametersSet()
        {
            NewPetStatusId = Item.PetStatusId;
        }

        public void Remove()
        {
        }

        public void Cancel()
        {
            NewPetStatusId = Item.PetStatusId;
        }

        public void Save()
        {

        }
    }
}
