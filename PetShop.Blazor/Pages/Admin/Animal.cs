﻿using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;
using PetShop.Blazor.Exceptions;
using PetShop.Blazor.Interops;
using PetShop.Blazor.Models;
using PetShop.Blazor.Models.Forms;
using PetShop.Blazor.Shared;
using PetShop.Blazor.States;
using System;
using System.Collections.Generic;

namespace PetShop.Blazor.Pages.Admin
{
    public partial class Animal : MasterComponentBase
    {
        [Inject]
        private IModalService ModalService { get; set; }


        public IEnumerable<AnimalModel> Items { get; set; }

        public AnimalForm NewItem { get; set; }


        public Animal()
        {
            NewItem = new AnimalForm();
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            Items = ModelState.Animals;
        }

        protected override void OnModelStateChanged()
        {
            Items = ModelState.Animals;
            StateHasChanged();
        }

        public async void Add()
        {
            try
            {
                await JsonClient.PostAsync("animal", NewItem);
                await JSRuntime.ToastrSuccessAsync($"{NewItem.Name} has been added", null);
                NewItem = new AnimalForm();
                ModelState.RefreshModelStateAsync();
                await JSRuntime.BsCloseModal("#addAnimalModal");
            }
            catch (HttpResponseException e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, e.StatusCode.ToString());
            }
            catch (Exception e)
            {
                await JSRuntime.ToastrErrorAsync(e.Message, "Unknown Exception");
            }
        }

        public async void Remove(AnimalModel animal)
        {
            var modalRef = ModalService.Show<ConfirmModal>("Confirm delete ?");
            var result = await modalRef.Result;
            if (!result.Cancelled)
            {
                try
                {
                    await JsonClient.DeleteAsync("animal/" + animal.Id);
                    await JSRuntime.ToastrSuccessAsync($"{animal.Name} has been removed", null);
                    ModelState.RefreshModelStateAsync();
                }
                catch (HttpResponseException e)
                {
                    await JSRuntime.ToastrErrorAsync(e.Message, e.StatusCode.ToString());
                }
                catch (Exception e)
                {
                    await JSRuntime.ToastrErrorAsync(e.Message, "Unknown Exception");
                }
            }
        }
    }
}
