﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using PetShop.Blazor.Abstractions.Interfaces;
using PetShop.Blazor.States;
using System;

namespace PetShop.Blazor.Pages
{
    public class MasterComponentBase : ComponentBase, IDisposable
    {
        [Inject]
        protected AppState AppState { get; set; }

        [Inject]
        protected ModelState ModelState { get; set; }

        [Inject]
        protected IJsonRequester JsonClient { get; set; }

        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected NavigationManager Navigation { get; set; }

        protected override void OnInitialized()
        {
            ModelState.ModelStateChanged += OnModelStateChanged;
            AppState.AppStateChanged += OnAppStateChanged;
        }

        protected virtual void OnAppStateChanged()
        {
        }

        protected virtual void OnModelStateChanged()
        {

        }

        public void Dispose()
        {
            ModelState.ModelStateChanged -= OnModelStateChanged;
            AppState.AppStateChanged -= OnAppStateChanged;
        }
    }
}
