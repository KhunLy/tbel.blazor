﻿window.consolelog = console.log

toastr.options = {
    "closeButton": true,
    "debug": true,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

window.toastr = {
    success : toastr.success,
    error : toastr.error,
    info : toastr.info
}

window.bs = {
    closeModal : (selector) => $(selector).modal("hide"),
    openModal : (selector) => $(selector).modal("show")
}

window.cropAsync = async (input, cropper, image) => {
    let $preview = new Croppie(cropper, {
        viewport: {
            width: 300,
            height: 225
        },
        boundary: {
            width: 360,
            height: 270
        },
        enableExif: true
    });

    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $preview.bind({
                url: e.target.result
            });
        };
        reader.readAsDataURL(input.files[0]);
    }

    let button = document.createElement('button');

    button.innerText = 'Save';

    button.classList.add('btn');
    button.classList.add('btn-outline-info');

    button.setAttribute('type', 'button');

    let parent = cropper.parentNode;
    parent.appendChild(button);

    return await new Promise((resolve, reject) => {
        button.addEventListener('click', e => {
            $preview.result('blob').then((blob) => {
                let blobUrl = URL.createObjectURL(blob);
                const reader = new FileReader();
                $(image).attr('src', blobUrl);
                reader.addEventListener('loadend', (e) => {
                    const text = e.target.result;
                    parent.removeChild(button);
                    $preview.destroy();
                    $(input).val('');
                    resolve(text);
                });
                reader.readAsDataURL(blob);
            });
        });
    });

}

window.trigger = {
    click: item => {
        item.click();
    }
}

