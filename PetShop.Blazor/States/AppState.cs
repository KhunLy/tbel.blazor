﻿using System;

namespace PetShop.Blazor.States
{
    public class AppState
    {
        private bool isLoading;
        public bool IsLoading
        {
            get { return isLoading; }
            set
            {
                if (value != isLoading)
                {
                    isLoading = value;
                    AppStateChanged?.Invoke();
                }
            }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                if (value != title)
                {
                    title = value;
                    AppStateChanged?.Invoke();
                }
            }
        }

        public event Action AppStateChanged;
    }
}
