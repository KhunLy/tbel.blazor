﻿using PetShop.Blazor.Abstractions.Interfaces;
using PetShop.Blazor.Models;
using System;
using System.Collections.Generic;

namespace PetShop.Blazor.States
{
    public class ModelState
    {
        public IEnumerable<AnimalModel> Animals { get; set; }
        public IEnumerable<BreedModel> Breeds { get; set; }
        public IEnumerable<PetStatusModel> PetStatuses { get; set; }


        private IJsonRequester JsonClient;

        public event Action ModelStateChanged;

        public ModelState(IJsonRequester jsonClient)
        {
            JsonClient = jsonClient;
            InitAsync();
        }

        private async void InitAsync()
        {
            PetStatuses = await JsonClient.GetAsync<IEnumerable<PetStatusModel>>("PetStatus");
            RefreshModelStateAsync();
        }

        public async void RefreshModelStateAsync()
        {
            Animals = await JsonClient.GetAsync<IEnumerable<AnimalModel>>("Animal");
            Breeds = await JsonClient.GetAsync<IEnumerable<BreedModel>>("Breed");
            ModelStateChanged?.Invoke();
        }
    }
}
