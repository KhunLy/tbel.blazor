﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using PetShop.Blazor.Services;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PetShop.Blazor.States
{
    public class AuthState : AuthenticationStateProvider
    {
        private readonly ILocalStorageService LocalStorage;

        private readonly JwtParser JwtParser;

        public AuthState(ILocalStorageService localStorage, JwtParser jwtParser)
        {
            LocalStorage = localStorage;
            LocalStorage.Changed += (sender, e) =>
            {
                NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
            };
            JwtParser = jwtParser;
        }

        public async override Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            ClaimsPrincipal principal = JwtParser.GetClaimsIdentity(await LocalStorage.GetItemAsync<string>("TOKEN"));
            return new AuthenticationState(principal);
        }
    }
}
