﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ToolBox.HttpJsonClient.Services
{
    public abstract class JsonRequester : IDisposable
    {
        private readonly HttpClient client;

        public JsonRequester(HttpClient client)
        {
            this.client = client;
        }

        public JsonRequester AddHeader(string header, string value)
        {
            client.DefaultRequestHeaders.Add(header, value);
            return this;
        }

        public async Task<TResult> GetAsync<TResult>(string url, CancellationToken ct = default)
        {
            try
            {
                await BeforeSendAsync();
                HttpResponseMessage response = await client.GetAsync(url, ct);
                return await HandleResponseAsync<TResult>(response);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                await AfterSendAsync();
            }
        }

        public async Task PostAsync<TBody>(string url, TBody body, CancellationToken ct = default)
        {
            try
            {
                await BeforeSendAsync();
                HttpResponseMessage response = await client.PostAsync(url, ToHttpContent(body), ct);
                await HandleResponseAsync(response);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                await AfterSendAsync();
            }
        }

        public async Task<TResult> PostAsync<TBody, TResult>(string url, TBody body, CancellationToken ct = default)
        {
            try
            {
                await BeforeSendAsync();
                HttpResponseMessage response = await client.PostAsync(url, ToHttpContent(body), ct);
                return await HandleResponseAsync<TResult>(response);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                await AfterSendAsync();
            }
        }

        public async Task PutAsync<TBody>(string url, TBody body, CancellationToken ct = default)
        {
            try
            {
                await BeforeSendAsync();
                HttpResponseMessage response = await client.PutAsync(url, ToHttpContent(body), ct);
                await HandleResponseAsync(response);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                await AfterSendAsync();
            }
        }

        public async Task<TResult> PutAsync<TBody, TResult>(string url, TBody body, CancellationToken ct = default)
        {
            try
            {
                await BeforeSendAsync();
                HttpResponseMessage response = await client.PutAsync(url, ToHttpContent(body), ct);
                return await HandleResponseAsync<TResult>(response);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                await AfterSendAsync();
            }
        }

        public async Task PatchAsync<TBody>(string url, TBody body, CancellationToken ct = default)
        {
            try
            {
                await BeforeSendAsync();
                HttpResponseMessage response = await client.SendAsync(
                    new HttpRequestMessage(new HttpMethod("PATCH"), url)
                    {
                        Content = ToHttpContent(body),
                    }, ct);
                await HandleResponseAsync(response);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                await AfterSendAsync();
            }
        }

        public async Task<TResult> PatchAsync<TBody, TResult>(string url, TBody body, CancellationToken ct = default)
        {
            try
            {
                await BeforeSendAsync();
                HttpResponseMessage response = await client.SendAsync(
                    new HttpRequestMessage(new HttpMethod("PATCH"), url)
                    {
                        Content = ToHttpContent(body),
                    }, ct);
                return await HandleResponseAsync<TResult>(response);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                await AfterSendAsync();
            }
        }

        public async Task DeleteAsync(string url, CancellationToken ct = default)
        {
            try
            {
                await BeforeSendAsync();
                HttpResponseMessage response = await client.DeleteAsync(url, ct);
                await HandleResponseAsync(response);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                await AfterSendAsync();
            }
        }

        public async Task<TResult> DeleteAsync<TResult>(string url, CancellationToken ct = default)
        {
            try
            {
                await BeforeSendAsync();
                HttpResponseMessage response = await client.DeleteAsync(url, ct);
                return await HandleResponseAsync<TResult>(response);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                await AfterSendAsync();
            }
        }

        protected virtual HttpContent ToHttpContent<TBody>(TBody body)
        {
            string json = JsonConvert.SerializeObject(body);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        protected async virtual Task HandleResponseAsync(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                IsUnsuccessStatusCode(response.StatusCode, content);
            }
        }

        protected virtual async Task<TResult> HandleResponseAsync<TResult>(HttpResponseMessage response)
        {
            string content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                IsUnsuccessStatusCode(response.StatusCode, content);
            }
            return JsonConvert.DeserializeObject<TResult>(content);
        }

        protected virtual Task BeforeSendAsync() { return Task.Delay(0); }

        protected virtual Task AfterSendAsync() { return Task.Delay(0); }

        protected abstract void IsUnsuccessStatusCode(HttpStatusCode code, string content);

        private bool disposed;

        public void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                client.Dispose();
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
