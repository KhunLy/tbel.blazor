﻿using System.Threading;
using System.Threading.Tasks;

namespace PetShop.Blazor.Abstractions.Interfaces
{
    public interface IJsonRequester
    {
        IJsonRequester AddBearer(string value);
        Task<TResult> GetAsync<TResult>(string url, CancellationToken ct = default);
        Task PostAsync<TBody>(string url, TBody body, CancellationToken ct = default);
        Task<TResult> PostAsync<TBody, TResult>(string url, TBody body, CancellationToken ct = default);
        Task PutAsync<TBody>(string url, TBody body, CancellationToken ct = default);
        Task<TResult> PutAsync<TBody, TResult>(string url, TBody body, CancellationToken ct = default);
        Task PatchAsync<TBody>(string url, TBody body, CancellationToken ct = default);
        Task<TResult> PatchAsync<TBody, TResult>(string url, TBody body, CancellationToken ct = default);
        Task DeleteAsync(string url, CancellationToken ct = default);
        Task<TResult> DeleteAsync<TResult>(string url, CancellationToken ct = default);
    }
}
